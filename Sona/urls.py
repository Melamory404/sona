from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import login, logout
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import re_path
from django.views.generic import RedirectView
from django.conf.urls import handler404, handler500
from tracker.mixins.access_mixins import error_404, error_500

from accounts.views import *
from tracker.views import *

handler404 = error_404
handler500 = error_500

urlpatterns = [
                  re_path(r'^admin/', admin.site.urls),
                  re_path(r'^favicon\.ico$', RedirectView.as_view(url='/staticfiles/img/favicon.ico', permanent=True)),

                  re_path(r'^$', RedirectView.as_view(url='/home/')),
                  re_path(r'^home/$', HomeView.as_view(), name='home'),
                  re_path(r'^calendar/$', CalendarView.as_view(), name='calendar'),
                  re_path(r'^users/$', ListUsersView.as_view(), name='users'),

                  re_path(r'^project/(?P<pk>\d+)/delete/$', DeleteProjectView.as_view(), name='project_delete'),
                  re_path(r'^task/(?P<pk>\d+)/(?P<project_pk>\d+)/delete/$', DeleteTaskView.as_view(),
                          name='task_delete'),
                  re_path(r'^task-notification/(?P<pk>\d+)/update/$', UpdateNotificationView.as_view(),
                          name='task_notification_update'),
                  re_path(r'^task-notification/(?P<pk>\d+)/(?P<task_pk>\d+)/delete/$', DeleteNotificationView.as_view(),
                          name='task_notification_delete'),
                  re_path(r'^project-access/(?P<pk>\d+)/(?P<project_pk>\d+)/delete/$',
                          DeleteProjectAccessView.as_view(),
                          name='project_access_delete'),
                  re_path(r'^task-access/(?P<pk>\d+)/(?P<task_pk>\d+)/delete/$', DeleteTaskAssignmentView.as_view(),
                          name='task_access_delete'),
                  re_path(r'^plan/(?P<pk>\d+)/(?P<project_pk>\d+)/delete/$', DeletePlanView.as_view(),
                          name='plan_delete'),

                  re_path(r'^task/search/$', SearchTaskView.as_view(), name='search_task'),
                  re_path(r'^task/(?P<pk>\d+)/(?P<project_pk>\d+)/complete/$', CompleteTaskView.as_view(),
                          name='complete_task'),
                  re_path(r'^task/(?P<pk>\d+)/(?P<project_pk>\d+)/restore/$', RestoreTaskView.as_view(),
                          name='restore_task'),

                  re_path(r'^project/new/$', NewProjectView.as_view(), name='new_project'),
                  re_path(r'^user/new/$', UserRegistrationView.as_view(), name='user_registration'),
                  re_path(r'^task/new/$', NewTaskView.as_view(), name='new_task'),
                  re_path(r'^sub-task/new/$', NewSubTaskView.as_view(), name='new_sub_task'),
                  re_path(r'^plan/new/$', NewPlanView.as_view(), name='new_plan'),
                  re_path(r'^comment/new/$', NewCommentView.as_view(), name='new_comment'),
                  re_path(r'^project-access/(?P<project_pk>\d+)/(?P<to_user_pk>\d+)/new/$',
                          NewProjectAccessView.as_view(), name='new_project_access'),
                  re_path(r'^task-access/(?P<task_pk>\d+)/(?P<to_user_pk>\d+)/new/$',
                          NewTaskAssignmentView.as_view(), name='new_task_access'),
                  re_path(r'^user-profile/new/$', NewUserProfileView.as_view(), name='new_user_profile'),
                  re_path(r'^notification/(?P<task_pk>\d+)/new/$', NewNotificationView.as_view(),
                          name='new_notification'),

                  re_path(r'^project/(?P<pk>\d+)/edit/$', ProjectUpdateView.as_view(), name='edit_project'),
                  re_path(r'^task/(?P<pk>\d+)/edit/$', TaskUpdateView.as_view(), name='edit_task'),

                  re_path(r'^project/(?P<pk>\d+)/access/$', DetailProjectAccessView.as_view(), name='project_access'),
                  re_path(r'^task/(?P<pk>\d+)/access/$', DetailTaskAssignmentView.as_view(), name='task_access'),

                  re_path(r'^user/(?P<pk>\d+)/$', DetailUserView.as_view(), name='user_detail'),
                  re_path(r'^project/(?P<pk>\d+)/$', DetailProjectView.as_view(), name='project_detail'),
                  re_path(r'^task/(?P<pk>\d+)/$', DetailTaskView.as_view(), name='task_detail'),
                  re_path(r'^project/(?P<project_pk>\d+)/plans', ProjectPlanView.as_view(), name='project_plans'),

                  re_path(r'^task/search/$', SearchTaskView.as_view(), name='search'),

                  re_path(r'^login/$', login, {'template_name': 'login.html'}, name='login'),
                  re_path(r'^logout/$', logout, {'next_page': '/login/'}, name='logout')
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += staticfiles_urlpatterns()

from django.db import models
from django.contrib.auth.models import User
import datetime

import pytz

utc = pytz.UTC

class Status(object):
    PENDING = 'Pending'
    FAILED = 'Failed'
    COMPLETED = 'Completed'

class NotificationEnum(object):
    minute = 'minute'
    hour = 'hour'
    day = 'day'
    week = 'week'

    @staticmethod
    def choices():
        choices = list()

        choices.append((NotificationEnum.minute, NotificationEnum.minute))
        choices.append((NotificationEnum.hour, NotificationEnum.hour))
        choices.append((NotificationEnum.day, NotificationEnum.day))
        choices.append((NotificationEnum.week, NotificationEnum.week))

        return choices

class RepeatEnum(object):
    day = 'Every day'
    week = 'Every week'
    month = 'Every month'
    year = 'Every year'

    actual_day = 'day'
    actual_week = 'week'
    actual_month = 'month'
    actual_year = 'year'

    @staticmethod
    def choices():
        choices = list()

        choices.append(('day', RepeatEnum.day))
        choices.append(('week', RepeatEnum.week))
        choices.append(('month', RepeatEnum.month))
        choices.append(('year', RepeatEnum.year))

        return choices


class Project(models.Model):
    title = models.CharField(max_length=100)

    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    date_create = models.DateTimeField(auto_now_add=True, editable=False)
    date_edit = models.DateTimeField(auto_now=True, null=True)

    status = models.CharField(max_length=100, default=Status.PENDING)

    def __str__(self):
        return 'Owner: {0} - Title: {1}'.format(self.owner.username, self.title)


class ProjectAccess(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='owner')
    on_project = models.ForeignKey(Project, on_delete=models.CASCADE)
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='to_user')

    def __str__(self):
        return 'From: {0} - To: {1} (Project id: {2}'\
            .format(self.owner.username, self.to_user.username, self.on_project.id)


class Tag(models.Model):
    tag = models.CharField(max_length=100)

    def __str__(self):
        return self.tag


class PriorityLevel(object):

    @staticmethod
    def choices():
        return list([(1, 'Low'), (2, 'Middle'), (3, 'High')])


class Attribute(object):
    @staticmethod
    def choices():
        return list([('Car', 'Car'), ('School', 'School'), ('Work', 'Work'), ('Home', 'Home')])



class Task(models.Model):
    title = models.CharField(max_length=100, null=True)
    attr = models.CharField(verbose_name='Attribute', max_length=100, choices=Attribute.choices(), null=True, blank=True)
    priority_level = models.IntegerField(choices=PriorityLevel.choices(), default=PriorityLevel.choices()[0][0], null=True, blank=True)
    status = models.CharField(max_length=100, default=Status.PENDING, null=True, blank=True)

    date_start = models.DateTimeField(null=True, blank=True)
    date_end = models.DateTimeField(null=True, blank=True)

    task_on = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    in_task_to = models.ForeignKey('self', null=True, on_delete=models.CASCADE)

    date_create = models.DateTimeField(auto_now_add=True, editable=False, null=True)
    date_edit = models.DateTimeField(auto_now=True, null=True)

    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return 'Owner: {0} - Title: {1}'.format(self.created_by.username, self.title)

    def complete_down(self):
        self.status = Status.COMPLETED
        sub_tasks = Task.objects.filter(in_task_to=self)
        self.save()
        for sub in sub_tasks:
            sub.complete_down()
            sub.save()

    def restore_up(self):
        self.status = Status.PENDING
        self.save()
        if self.in_task_to is not None:
            self.in_task_to.restore_up()

class TaskAssignment(models.Model):
    owner = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='task_owner')
    on_task = models.ForeignKey(Task, on_delete=models.CASCADE)
    to_user = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='to_task_user')

    def __str__(self):
        return 'From: {0} - To: {1} (Task id: {2}' \
            .format(self.owner.username, self.to_user.username, self.on_task.id)


class Notification(models.Model):
    notification_on = models.ForeignKey(Task, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True)
    is_seen = models.BooleanField(default=False)

    notify_date = models.DateTimeField(null=True)

class Comment(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    on_task = models.ForeignKey(Task, on_delete=models.CASCADE)
    reply_comment = models.ForeignKey('self', null=True, on_delete=models.CASCADE)
    comment_msg = models.CharField(max_length=100)

    def __str__(self):
        return 'Owner: {0} - Comment: {1}'.format(self.owner.username, self.comment_msg)

class Plan(models.Model):
    plan_on_project = models.ForeignKey(Project, on_delete=models.CASCADE)
    plan_on_task = models.ForeignKey(Task, on_delete=models.CASCADE, null=True)
    plan_date_repeat_end = models.DateTimeField(null=True, blank=True)
    plan_policy_repeat = models.CharField(max_length=100, choices=RepeatEnum.choices(), null=True, blank=True)
    plan_owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)



class Role(models.Model):
    giver = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='giver', null=True)
    on_user = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='receiver', null=True)
    on_project = models.ForeignKey(Project, on_delete=models.DO_NOTHING)
    role = models.CharField(max_length=100)


class CheckList(models.Model):
    name = models.CharField(max_length=255)
    options = models.ManyToManyField('CheckListOption', blank=True)

    def __unicode__(self):
        return self.name


class CheckListOption(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name
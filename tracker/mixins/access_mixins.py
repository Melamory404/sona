from django.contrib.auth.mixins import AccessMixin
from django.shortcuts import get_object_or_404, reverse, render
from django.contrib import messages
from django.http.response import HttpResponseRedirect
from tracker.models import Task, TaskAssignment, Project, ProjectAccess



class TaskPermissionMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if self.user_has_permissions(request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)
        messages.error(request, 'Some error appeared. Sorry !')
        return HttpResponseRedirect(reverse('home'))

    def user_has_permissions(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=kwargs['pk'])

        allow = Task.objects.filter(id=task.pk, created_by=request.user).exists()
        is_admin = Task.objects.filter(id=task.pk, task_on__owner=request.user).exists()
        is_assign = TaskAssignment.objects.filter(on_task=task, to_user=request.user).exists()

        if allow or is_admin or is_assign:
            return True
        else:
            return False

class SubTaskPermissionMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if self.user_has_permissions(request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)
        messages.error(request, 'Some error appeared. Sorry !')
        return HttpResponseRedirect(reverse('home'))

    def user_has_permissions(self, request, *args, **kwargs):
        parent_task_pk = request.GET['parent_task_pk']

        task = Task.objects.filter(id=parent_task_pk).first()
        is_assign = TaskAssignment.objects.filter(on_task=task, to_user=request.user).exists()
        allow = Task.objects.filter(id=parent_task_pk, created_by=request.user.id).exists()
        is_admin = Task.objects.filter(id=parent_task_pk, task_on__owner=request.user).exists()

        if allow or is_admin or is_assign:
            return True
        else:
            return False

class ProjectPermissionMixin(AccessMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if self.user_has_permissions(request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)
        messages.error(request, 'Some error appeared. Sorry !')
        return HttpResponseRedirect(reverse('home'))

    def user_has_permissions(self, request, *args, **kwargs):
        project_pk = kwargs['pk']

        allow = Project.objects.filter(id=project_pk, owner=request.user).exists()
        is_access = ProjectAccess.objects.filter(to_user=request.user, on_project=project_pk).exists()

        if allow or is_access:
            return True
        else:
            return False


def error_404(request):
    messages.error(request, 'Some error appeared. Sorry !')
    return render(request, 'home.html', status=404)

def error_500(request):
    messages.error(request, 'Some error appeared. Sorry !')
    return render(request, 'home.html', status=500)
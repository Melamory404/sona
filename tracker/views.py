from django.contrib.auth.mixins import LoginRequiredMixin
from tracker.mixins.access_mixins import *
from django.http.response import HttpResponseRedirect
from django.shortcuts import reverse, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, DetailView, DeleteView, UpdateView, ListView

from tracker.forms import *
from tracker.models import *
import pytz

utc = pytz.UTC


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        ctx = super(HomeView, self).get_context_data(**kwargs)

        projects = Project.objects.filter(owner=self.request.user)
        ctx['projects'] = projects

        access_projects = Project.objects.filter(projectaccess__to_user=self.request.user)

        ctx['access_projects'] = access_projects

        return ctx


class CalendarView(LoginRequiredMixin, TemplateView):
    template_name = 'calendar.html'

    def get_context_data(self, **kwargs):
        ctx = super(CalendarView, self).get_context_data(**kwargs)
        tasks = Task.objects.filter(created_by=self.request.user, date_start__isnull=False, date_end__isnull=False)
        ctx['tasks'] = tasks
        return ctx


class NewProjectView(LoginRequiredMixin, CreateView):
    model = Project
    fields = ('title',)
    template_name = 'new_project.html'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.object = None

    def form_valid(self, form):
        new_project = form.save(commit=False)
        new_project.owner = self.request.user
        new_project.save()

        self.object = new_project
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('project_detail', kwargs={'pk': self.object.pk})


class NewPlanView(LoginRequiredMixin, CreateView):
    form_class = PlanModelForm
    http_method_names = ('post',)
    template_name = 'new_plan.html'

    def form_valid(self, form):
        on_project = Project.objects.get(pk=form.cleaned_data['project_pk'])

        new_plan = form.save(commit=False)
        new_plan.plan_on_project = on_project
        new_plan.plan_on_task = form.cleaned_data['on_task']
        new_plan.plan_owner = self.request.user
        new_plan.save()

        return HttpResponseRedirect(reverse('project_detail', kwargs={'pk': on_project.pk}))

    def get_initial(self):
        initial_data = super(NewPlanView, self).get_initial()
        initial_data['project_pk'] = self.request.GET['project_pk']
        return initial_data

    def get_context_data(self, **kwargs):
        ctx = super(NewPlanView, self).get_context_data(**kwargs)
        ctx['project'] = Project.objects.get(pk=self.request.GET['project_pk'])
        return ctx


class ProjectPlanView(LoginRequiredMixin, TemplateView):
    model = Plan
    template_name = 'plan_list.html'

    def get_context_data(self, **kwargs):
        ctx = super(ProjectPlanView, self).get_context_data(**kwargs)
        plans = Plan.objects.filter(plan_on_project=int(kwargs['project_pk']))
        ctx['plans'] = plans
        return ctx


class NewTaskView(LoginRequiredMixin, CreateView):
    form_class = TaskModelForm
    http_method_names = ('post',)
    template_name = 'new_task.html'

    def form_valid(self, form):
        parent_project = Project.objects.get(pk=form.cleaned_data['project_pk'])

        new_task = form.save(commit=False)
        new_task.task_on = parent_project
        new_task.created_by = self.request.user

        new_task.save()

        return HttpResponseRedirect(reverse('project_detail', kwargs={'pk': parent_project.pk}))

    def get_initial(self):
        initial_data = super(NewTaskView, self).get_initial()
        initial_data['project_pk'] = self.request.GET['project_pk']

        return initial_data

    def get_context_data(self, **kwargs):
        ctx = super(NewTaskView, self).get_context_data(**kwargs)
        ctx['project'] = Project.objects.get(pk=self.request.GET['project_pk'])

        return ctx


class NewProjectAccessView(LoginRequiredMixin, CreateView):
    http_method_names = ('post',)
    template_name = 'project_grant_access.html'

    def post(self, request, *args, **kwargs):
        new_access = ProjectAccess()
        new_access.owner = self.request.user

        new_access.to_user = User.objects.get(pk=kwargs['to_user_pk'])
        new_access.on_project = Project.objects.get(pk=kwargs['project_pk'])
        new_access.save()

        return HttpResponseRedirect(reverse('project_access', kwargs={'pk': kwargs['project_pk']}))


class CompleteTaskView(TaskPermissionMixin, UpdateView):
    model = Task
    http_method_names = ('post',)

    def post(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=kwargs['pk'])
        task.complete_down()

        return HttpResponseRedirect(reverse('project_detail', kwargs={'pk': kwargs['project_pk']}))


class RestoreTaskView(TaskPermissionMixin, UpdateView):
    model = Task
    http_method_names = ('post',)

    def post(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=kwargs['pk'])
        if task.date_end is not None and task.date_end.replace(tzinfo=utc) < datetime.datetime.now().replace(
                tzinfo=utc):
            task.status = Status.COMPLETED
            messages.error(request, "Can't restore task to PENDING status cause of deadline !")
        else:
            task.restore_up()
        task.save()

        return HttpResponseRedirect(reverse('project_detail', kwargs={'pk': kwargs['project_pk']}))


class NewTaskAssignmentView(LoginRequiredMixin, CreateView):
    http_method_names = ('post',)
    template_name = 'project_grant_access.html'

    def post(self, request, *args, **kwargs):
        new_access = TaskAssignment()
        new_access.owner = self.request.user
        new_access.to_user = User.objects.get(pk=kwargs['to_user_pk'])
        new_access.on_task = Task.objects.get(pk=kwargs['task_pk'])
        new_access.save()

        return HttpResponseRedirect(reverse('task_access', kwargs={'pk': kwargs['task_pk']}))


class NewSubTaskView(SubTaskPermissionMixin, CreateView):
    form_class = TaskModelForm
    template_name = 'new_sub_task.html'

    def get_context_data(self, **kwargs):
        ctx = super(NewSubTaskView, self).get_context_data(**kwargs)
        ctx['parent_task'] = Task.objects.get(pk=self.request.GET['parent_task_pk'])

        return ctx

    def get_initial(self):
        initial_data = super(NewSubTaskView, self).get_initial()

        project_pk = self.request.GET['project_pk']
        initial_data['project_pk'] = project_pk

        parent_task_pk = self.request.GET['parent_task_pk']
        initial_data['parent_task_pk'] = parent_task_pk

        return initial_data

    def form_valid(self, form):
        parent_project = Project.objects.get(pk=form.cleaned_data['project_pk'])
        parent_task = Task.objects.get(pk=form.cleaned_data['parent_task_pk'])

        new_task = form.save(commit=False)
        new_task.task_on = parent_project
        new_task.in_task_to = parent_task
        new_task.created_by = self.request.user

        new_task.save()

        return HttpResponseRedirect(reverse('project_detail', kwargs={'pk': parent_project.pk}))


class NewCommentView(LoginRequiredMixin, CreateView):
    form_class = CommentModelForm
    template_name = 'new_comment.html'
    http_method_names = ('post',)

    def get_initial(self):
        initial_data = super(NewCommentView, self).get_initial()

        task_pk = self.request.GET['task_pk']
        initial_data['task_pk'] = task_pk

        return initial_data

    def form_valid(self, form):
        parent_task = Task.objects.get(pk=form.cleaned_data['task_pk'])

        new_comment = form.save(commit=False)
        new_comment.on_task = parent_task
        new_comment.owner = self.request.user

        new_comment.save()

        return HttpResponseRedirect(reverse('task_detail', kwargs={'pk': parent_task.pk}))


class NewNotificationView(LoginRequiredMixin, CreateView):
    model = Notification
    form_class = NotificationModelForm
    template_name = 'new_notification.html'

    def dispatch(self, request, *args, **kwargs):
        task_pk = kwargs['task_pk']

        allow = Task.objects.filter(id=task_pk, created_by=self.request.user).exists()
        is_admin = Task.objects.filter(id=task_pk, task_on__owner=self.request.user).exists()

        if allow or is_admin:
            return super(NewNotificationView, self).dispatch(request, *args, **kwargs)
        else:
            messages.error(request, "Can't create notification to this task. Sorry !")
            return HttpResponseRedirect(reverse('home'))

    def get_context_data(self, **kwargs):
        ctx = super(NewNotificationView, self).get_context_data(**kwargs)
        ctx['form'].fields['task_pk'].initial = self.kwargs['task_pk']

        return ctx

    def form_valid(self, form):
        new_notification = form.save(commit=False)
        new_notification.notification_on = get_object_or_404(Task, pk=self.kwargs['task_pk'])
        new_notification.owner = self.request.user
        new_notification.save()

        return HttpResponseRedirect(reverse('task_detail', kwargs={'pk': self.kwargs['task_pk']}))


class DeleteProjectView(ProjectPermissionMixin, DeleteView):
    model = Project
    success_url = reverse_lazy('home')
    template_name = 'project_confirm_delete.html'


class DeleteTaskView(TaskPermissionMixin, DeleteView):
    model = Task
    template_name = 'task_confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('project_detail', kwargs={'pk': self.kwargs.get('project_pk')})


class DeleteNotificationView(LoginRequiredMixin, DeleteView):
    model = Notification

    def get_success_url(self):
        return reverse_lazy('task_detail', kwargs={'pk': self.kwargs.get('task_pk')})


class DeleteProjectAccessView(LoginRequiredMixin, DeleteView):
    model = ProjectAccess

    def get_success_url(self):
        return reverse_lazy('project_access', kwargs={'pk': self.kwargs.get('project_pk')})


class DeleteTaskAssignmentView(LoginRequiredMixin, DeleteView):
    model = TaskAssignment

    def get_success_url(self):
        return reverse_lazy('task_access', kwargs={'pk': self.kwargs.get('task_pk')})


class DeletePlanView(LoginRequiredMixin, DeleteView):
    model = Plan

    def get_success_url(self):
        return reverse_lazy('project_detail', kwargs={'pk': self.kwargs.get('project_pk')})


class UpdateNotificationView(LoginRequiredMixin, UpdateView):
    model = Notification
    http_method_names = ('post',)

    def get_success_url(self):
        return self.request.GET['next']

    def post(self, request, *args, **kwargs):
        notification = get_object_or_404(Notification, pk=kwargs['pk'])
        notification.is_seen = True
        notification.save()

        return HttpResponseRedirect('/home/')


class DetailProjectView(ProjectPermissionMixin, DetailView):
    model = Project
    template_name = 'project_detail.html'

    def get_context_data(self, **kwargs):
        ctx = super(DetailProjectView, self).get_context_data(**kwargs)
        project_tasks = Task.objects.filter(task_on=self.object, in_task_to__isnull=True).order_by('-priority_level').select_related('created_by')

        ctx['tasks'] = project_tasks
        ctx['task_form'] = TaskModelForm(initial={'project_pk': self.object.pk})
        ctx['plan_form'] = PlanModelForm(initial={'project_pk': self.object.pk})

        return ctx


class DetailProjectAccessView(ProjectPermissionMixin, DetailView):
    model = Project
    template_name = 'project_access.html'

    def get_context_data(self, **kwargs):
        ctx = super(DetailProjectAccessView, self).get_context_data(**kwargs)

        access_list = ProjectAccess.objects.filter(on_project=self.object)
        ctx['access_list'] = access_list

        users_with_access = User.objects.filter(
            id__in=access_list.exclude(to_user=self.object.owner).values_list('to_user', flat=True))
        users_without_access = User.objects.exclude(id__in=users_with_access).exclude(id=self.object.owner.id)

        ctx['users_with_access'] = users_with_access
        ctx['users_without_access'] = users_without_access

        return ctx


class DetailTaskAssignmentView(LoginRequiredMixin, DetailView):
    model = Task
    template_name = 'task_assignment.html'

    def get_context_data(self, **kwargs):
        ctx = super(DetailTaskAssignmentView, self).get_context_data(**kwargs)
        project_users = ProjectAccess.objects.filter(on_project=self.object.task_on).values_list('to_user', flat=True)
        access_list = TaskAssignment.objects.filter(on_task=self.object)
        ctx['access_list'] = access_list

        users_with_access = User.objects.filter(
            id__in=access_list.values_list('to_user', flat=True))
        users_without_access = User.objects.filter(id__in=project_users).exclude(id__in=users_with_access).exclude(
            id=self.object.created_by.id)

        ctx['users_with_access'] = users_with_access
        ctx['users_without_access'] = users_without_access

        return ctx


class DetailTaskView(TaskPermissionMixin, DetailView):
    model = Task
    template_name = 'task_detail.html'

    def get_context_data(self, **kwargs):
        ctx = super(DetailTaskView, self).get_context_data(**kwargs)

        comments = Comment.objects.filter(on_task=self.object)
        all_notifications = Notification.objects.filter(notification_on=self.object)

        ctx['comments'] = comments
        ctx['all_notifications'] = all_notifications
        ctx['comment_form'] = CommentModelForm(initial={'task_pk': self.object.pk})

        return ctx


class ProjectUpdateView(ProjectPermissionMixin, UpdateView):
    model = Project
    fields = ['title']
    template_name = 'edit_project.html'
    success_url = '/home/'


class TaskUpdateView(TaskPermissionMixin, UpdateView):
    model = Task
    form_class = TaskModelForm
    template_name = 'edit_task.html'

    def get_object(self, queryset=None):
        task = get_object_or_404(Task, pk=self.kwargs['pk'])
        return task

    def get_context_data(self, **kwargs):
        ctx = super(TaskUpdateView, self).get_context_data(**kwargs)
        task = get_object_or_404(Task, pk=self.kwargs['pk'])
        ctx['project'] = task.task_on
        return ctx

    def get_success_url(self):
        return reverse_lazy('project_detail', kwargs={'pk': self.object.task_on.pk})


class SearchTaskView(LoginRequiredMixin, TemplateView):
    template_name = 'task_search.html'

    def get_context_data(self, **kwargs):
        ctx = super(SearchTaskView, self).get_context_data(**kwargs)

        access_tasks = list()
        attr = self.request.GET.get('attribute')
        question = self.request.GET.get('question')

        accesses = ProjectAccess.objects.filter(to_user=self.request.user)

        if attr == '':
            tasks = Task.objects.filter(title__iexact=question,
                                        created_by=self.request.user)
            for access in accesses:
                temp_list = Task.objects.filter(task_on=access.on_project,
                                                title__exact=question)
                access_tasks.extend(temp_list)

        elif question == '':
            tasks = Task.objects.filter(created_by=self.request.user, attr__iexact=attr)
            for access in accesses:
                temp_list = Task.objects.filter(task_on=access.on_project,
                                                attr__iexact=attr)
                access_tasks.extend(temp_list)

        else:
            tasks = Task.objects.filter(title__iexact=question,
                                        created_by=self.request.user, attr__iexact=attr)
            for access in accesses:
                temp_list = Task.objects.filter(task_on=access.on_project,
                                                title__exact=question, attr__iexact=attr)
                access_tasks.extend(temp_list)

        ctx['tasks'] = tasks
        ctx['access_tasks'] = access_tasks

        return ctx

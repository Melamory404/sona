from django import forms
from tracker.models import Task, Comment, Notification, Plan
from django.contrib.admin import widgets

import pytz

utc = pytz.UTC


class PlanModelForm(forms.ModelForm):
    project_pk = forms.IntegerField(widget=forms.HiddenInput)
    on_task = forms.ModelChoiceField(queryset=Task.objects.all(), required=True)

    def __init__(self, *args, **kwargs):
        super(PlanModelForm, self).__init__(*args, **kwargs)
        self.fields['plan_date_repeat_end'].widget = forms.DateTimeInput(
            attrs={'placeholder': 'MM/DD/YY HH:MM:SS'})
        self.fields['plan_date_repeat_end'].widget.attrs['class'] = 'datetimepicker'

        task_queryset = Task.objects.filter(task_on=kwargs['initial']['project_pk'], in_task_to__isnull=True, date_start__isnull=False, date_end__isnull=False)
        self.fields['on_task'].queryset = task_queryset
        self.fields['on_task'].widget.choices = self.fields['on_task'].choices

        self.fields['plan_policy_repeat'].required = True
        self.fields['plan_date_repeat_end'].required = True

    class Meta:
        model = Plan
        fields = ('plan_date_repeat_end', 'plan_policy_repeat')

    def clean(self):
        super(PlanModelForm, self).clean()
        task = self.cleaned_data['on_task']
        date_repeat_end = self.cleaned_data['plan_date_repeat_end']
        if task.date_end > date_repeat_end:
            raise forms.ValidationError("Repeat end date can't be greater then end date.")


class TaskModelForm(forms.ModelForm):
    project_pk = forms.IntegerField(widget=forms.HiddenInput, required=False)
    parent_task_pk = forms.IntegerField(widget=forms.HiddenInput, required=False)

    def __init__(self, *args, **kwargs):
        super(TaskModelForm, self).__init__(*args, **kwargs)

        self.fields['date_start'].widget = forms.DateTimeInput(
            attrs={'placeholder': 'MM/DD/YY HH:MM:SS'})
        self.fields['date_start'].widget.attrs['class'] = 'datetimepicker'

        self.fields['date_end'].widget = forms.DateTimeInput(
            attrs={'placeholder': 'MM/DD/YY HH:MM:SS'})
        self.fields['date_end'].widget.attrs['class'] = 'datetimepicker'

    class Meta:
        model = Task
        fields = ('title', 'attr', 'priority_level', 'date_start', 'date_end')

    def clean(self):
        super(TaskModelForm, self).clean()
        date_start = self.cleaned_data['date_start']
        date_end = self.cleaned_data['date_end']

        if (date_start is None and date_end is not None) or (date_start is not None and date_end is None):
            raise  forms.ValidationError("Fill all date's pls !")

        if date_start is not None and date_end is not None:
            if date_start > date_end:
                raise forms.ValidationError("Start date can't be greater then end date.")

class CommentModelForm(forms.ModelForm):
    task_pk = forms.IntegerField(widget=forms.HiddenInput)
    parent_comment_pk = forms.IntegerField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = Comment
        fields = ('comment_msg',)


class NotificationModelForm(forms.ModelForm):
    task_pk = forms.IntegerField(widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        super(NotificationModelForm, self).__init__(*args, **kwargs)

        self.fields['notify_date'].widget = forms.DateTimeInput(
            attrs={'placeholder': 'MM/DD/YY HH:MM:SS'})
        self.fields['notify_date'].widget.attrs['class'] = 'datetimepicker'

    class Meta:
        model = Notification
        fields = ('notify_date',)

    def clean(self):
        super(NotificationModelForm, self).clean()
        notify_date = self.cleaned_data['notify_date']
        task = Task.objects.get(id=self.cleaned_data['task_pk'])
        if task.date_end is not None:
            if notify_date > task.date_end:
                raise forms.ValidationError("Notify date can't be greater then End date !")

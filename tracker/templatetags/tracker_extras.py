from django import template

register = template.Library()


@register.filter(name='is_empty')
def is_empty(value):
    if isinstance(value, list):
        if len(value) > 0:
            return True
    return False

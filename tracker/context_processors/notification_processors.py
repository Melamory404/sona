from tracker.models import Notification, Task, Attribute, Status, Plan, RepeatEnum
from django.contrib.auth.models import AnonymousUser
from calendar import monthrange
import datetime
import pytz

utc = pytz.UTC


def notify_user(request):
    user = request.user
    if not isinstance(user, AnonymousUser):
        notifications = Notification.objects.filter(notify_date__lte=utc.localize(datetime.datetime.now()), owner=user,
                                                    is_seen=False)
        return {'notifications': notifications}
    return Notification.objects.none()


def attributes(request):
    return {'attributes': Attribute.choices()}


def check_deadlines(request):
    user = request.user
    if not isinstance(user, AnonymousUser):
        tasks_with_passed_deadline = Task.objects.filter(created_by=user,
                                                         date_end__lte=datetime.datetime.now().replace(tzinfo=utc))
        for task in tasks_with_passed_deadline:
            if task.status == Status.PENDING:
                task.status = Status.FAILED
                task.save()
    return Task.objects.none()


def days_in_year(year):
    if year % 4 != 0:
        return 365
    elif year % 100 == 0:
        if year % 400 == 0:
            return 366
        else:
            return 365
    else:
        return 366


def days_in_month(month, year):
    days = monthrange(year, month)
    return days[1]


def repeat_tasks(request):
    user = request.user
    if not isinstance(user, AnonymousUser):
        plans = Plan.objects.filter(plan_owner=user)
        repeat_value = 1
        for plan in plans:
            notifications = Notification.objects.filter(notification_on=plan.plan_on_task)
            date_now = datetime.datetime.now()
            if plan.plan_policy_repeat == RepeatEnum.actual_day:
                repeat_value = 1
            elif plan.plan_policy_repeat == RepeatEnum.actual_week:
                repeat_value = 7
            elif plan.plan_policy_repeat == RepeatEnum.actual_month:
                repeat_value = days_in_month(date_now.month, date_now.year)
            elif plan.plan_policy_repeat == RepeatEnum.actual_year:
                repeat_value = days_in_year(date_now.year)
            deadline = plan.plan_on_task.date_start + datetime.timedelta(days=repeat_value)
            while deadline < utc.localize(date_now):
                if deadline < utc.localize(date_now):
                    plan.plan_on_task.status = Status.PENDING
                    plan.plan_on_task.date_start = deadline
                    plan.plan_on_task.date_end = plan.plan_on_task.date_end + datetime.timedelta(days=repeat_value)
                else:
                    plan.plan_policy_repeat = None
                    plan.plan_date_repeat_end = None
                for notification in notifications:
                    notification.notify_date += datetime.timedelta(days=repeat_value)
                    notification.is_seen = False
                    notification.save()
                plan.plan_on_task.save()
                plan.save()
                deadline = plan.plan_on_task.date_start + datetime.timedelta(days=repeat_value)
    return Task.objects.none()

$(document).ready(function () {

    $(".highlight-hover-list li").hover(function () {
        $(this).addClass('active');
    }, function () {
        $(this).removeClass('active');
    });
    $('.datetimepicker').datetimepicker({
        format: 'm/d/Y h:m:s'
    });

    notification_checkbox_1 = $('#notificationCheckBox1');
    notification_checkbox_2 = $('#notificationCheckBox2');
    notification_form_1 = $('#notificationForm1');
    notification_form_2 = $('#notificationForm2');

    notification_checkbox_1.prop('checked', true);

    notification_checkbox_1.on('click', function () {
        if ($(this).is(':checked')) {
            notification_form_1.show();
            notification_form_2.hide();
            notification_checkbox_2.prop('checked', false);
        }
        else {
            notification_form_1.hide();
            notification_form_2.show();
            notification_checkbox_2.prop('checked', true);
        }
    });
    notification_checkbox_2.on('click', function () {
        if ($(this).is(':checked')) {
            notification_form_2.show();
            notification_form_1.hide();
            notification_checkbox_1.prop('checked', false);
        }
        else {
            notification_form_2.hide();
            notification_form_1.show();
            notification_checkbox_1.prop('checked', true);
        }
    });
    policy_repeat_field = $('.policy-repeat');
    policy_repeat_field.prop('disabled', 'disabled');

    $('.repeat-end').datetimepicker({
        format: 'm/d/Y h:m:s',
        onChangeDateTime: function (dp, $input) {
            console.log($(this).val());
            if ($input.val().length !== 0) {
                policy_repeat_field.prop('disabled', null);
            }
            else {
                policy_repeat_field.prop('disabled', 'disabled');
            }
        }
    });

    function getRandomColor() {
        let letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }


    let tasks = [];
    $('.tasks').each(function () {
        $(this).find('li').each(function () {
            task_title = $(this).find('.task-title');
            task_date_start = $(this).find('.task-date-start');
            task_date_end = $(this).find('.task-date-end');
            console.log(task_title.text());
            tasks.push({
                start: task_date_start.text(),
                end: task_date_end.text(),
                title: task_title.text(),
                url: '#',
                class: 'custom-class',
                color: getRandomColor(),
                data: {
                },
            });
        });
    });

    $('.event-calendar').equinox({
        events: tasks,
    });


});

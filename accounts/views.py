from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http.response import HttpResponseRedirect
from django.shortcuts import reverse
from django.views.generic import CreateView, DetailView, ListView

from accounts.forms import UserProfileForm
from accounts.models import UserProfile, default_avatar
from tracker.models import Project


class UserRegistrationView(CreateView):
    form_class = UserCreationForm
    template_name = 'user_registration.html'
    success_url = '/home/'

    def form_valid(self, form):
        response = super(UserRegistrationView, self).form_valid(form)
        profile = UserProfile()
        profile.user = form.instance
        profile.save()
        return response


class NewUserProfileView(LoginRequiredMixin, CreateView):
    form_class = UserProfileForm
    template_name = 'new_user_profile.html'
    http_method_names = ('post',)

    def form_valid(self, form):
        user_profile = form.save(commit=False)
        user_profile.user = self.request.user
        clean_user_profile = UserProfile.objects.filter(user=self.request.user)
        if clean_user_profile.exists():
            avatar_path = clean_user_profile.first().avatar.path
            avatar_path = avatar_path.split('/')

            if not avatar_path[-1] == default_avatar:
                clean_user_profile.first().avatar.delete()

            clean_user_profile.delete()
        user_profile.save()

        return HttpResponseRedirect(reverse('user_detail', kwargs={'pk': self.request.user.id}))


class DetailUserView(DetailView):
    model = User
    template_name = 'user_detail.html'

    def get_context_data(self, **kwargs):
        ctx = super(DetailUserView, self).get_context_data(**kwargs)

        projects = Project.objects.filter(owner=self.object)
        profile = UserProfile.objects.get(user=self.request.user)
        ctx['profile'] = profile
        ctx['projects'] = projects
        ctx['user_profile_form'] = UserProfileForm()
        return ctx


class ListUsersView(ListView):
    model = UserProfile
    template_name = 'users.html'


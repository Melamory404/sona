from django.contrib.auth.models import User
from django.db import models
import os
import Sona.settings as settings

default_avatar = 'default.jpg'

class UserProfile(models.Model):
 #   also = models.CharField(max_length=10, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(verbose_name='Avatar',
                               upload_to='Avatars',
                               default=os.path.join(settings.MEDIA_ROOT, 'Avatars', default_avatar),
                               null=True,
                               blank=True)


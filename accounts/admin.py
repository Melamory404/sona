from django.contrib import admin

from tracker.models import *

admin.site.register(Project)
admin.site.register(ProjectAccess)
admin.site.register(Tag)
admin.site.register(Task)
admin.site.register(Notification)
admin.site.register(Comment)
admin.site.register(Role)
